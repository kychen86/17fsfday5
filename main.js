//Load express library
var express = require("express");
//Create an instance of the app
var app = express();

//Create a route for randomimg
//Version 1
var generatingImg = function(res){
    var imgIdx = Math.round(Math.random()*30);
    res.status(200);
    res.type("test/html");
    res.send("<img src='/images/number" + imgIdx + ".jpg' height ='300px'>");
}

app.get("randomimg",function(req, res){
    generateImg(res); //Send a function that returns a html name ie: url of image
})

//Mount public as a static resource directory
app.use(express.static(__dirname +"/public"));

// The final picture is generated using the app.use to grab the html of the image file, the above just returns a html name.
//File is in public, images, with file name number##.jpg


//Version 2 This allows for embed into the html
var loadImage =function(res){
    var imgIdx = Math.round(Math.random()*30);
    res.status(200);
    res.type("image/jpg")
    res.sendFile(__dirname + "/public/images/number" + imgIdx + ".jpg");   
}

app.get("/randomimg",function(req,res){
    loadImage(res);
});

//Version 1 request gives a return of of the html of the image, then request again the exact location of the image, a redirect without randomimg page created
//Version 2 request the image and post as randomimg page. Can be embeded in other pages of the html.

//Port stuff
var port = process.argv[2]||3000;
app.listen(port,function(){
    console.log("Application started on port %d", port)
});

